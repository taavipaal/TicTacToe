﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace TripsTrapsTrull
{
    class Program
    {
        static void Main(string[] args)
        {
            // Intro, küsib mängijate nimesid
            Console.WriteLine("Mängija 1, mis on su nimi?");
            string player1 = Console.ReadLine();

            Console.WriteLine("Mängija 2, mis on sinu nimi?");
            string player2 = Console.ReadLine();

            Console.WriteLine("Trips-Traps-Trull algab!");

            // algseis
            List<string> LegalMoves = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            List<string> DoneMoves = new List<string>();

            // ruudustiku moodustamine
            string ruudustik = "   |   |   ---+---+---   |   |   ---+---+---   |   |   ";
            char[] GameTable = ruudustik.ToCharArray();

            // -1, kuna array algab nullist
            int[] xoPositsioonid = { 1,  5,  9, 23, 27, 31, 45, 49, 53 };

            // win conditions
            string X = "XXX";
            string O = "OOO";

            // vahekokkuvõte
            char[] SubTotal = new char[9];

            // MÄNG ALGAB
            bool GameOn = true;
            bool player = true; // true on player 1

            while (GameOn)
            {
                if (player)
                {
                    Console.WriteLine($"{player1}, sinu kord!");
                }
                else
                {
                    Console.WriteLine($"{player2}, sinu kord!");
                }

                // 3x3 ruudustik
                int counter = 0;
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 11; j++)
                    {
                        Console.Write(GameTable[j + counter]);
                    }
                    counter += 11;
                    Console.WriteLine();
                }

                // kontrollib, kui on 9 käiku tehtud, siis on viik
                if (DoneMoves.Count == 9)
                {
                    Console.WriteLine("Viik!");
                    GameOn = false;
                }
                else
                {
                    // mängija vajutab nuppu, käik salvestatakse
                    string ButtonPressed = Console.ReadLine();

                    // kontroll, kas käik on legaalne ning kas keegi pole juba seda enne käinud
                    if (!LegalMoves.Contains(ButtonPressed) || DoneMoves.Contains(ButtonPressed))
                    {
                        Console.WriteLine("Sellist käiku ei saa teha!");
                        Console.WriteLine("Proovi uuesti!");
                    }
                    else
                    {
                        DoneMoves.Add(ButtonPressed);

                        if (player)
                        {
                            // -1, kuna array algab nullist ning nupuvajutused algavad 1st
                            GameTable[xoPositsioonid[int.Parse(ButtonPressed) - 1]] = 'X';
                            player = false;
                        }
                        else
                        {
                            GameTable[xoPositsioonid[int.Parse(ButtonPressed) - 1]] = 'O';
                            player = true;
                        }

                        // teha vahearray, kus on gametable väärtused aga ainult X ja O

                        for (int i = 0; i < xoPositsioonid.Length; i++)
                        {
                            SubTotal[i] = GameTable.ElementAt(xoPositsioonid[i]);   
                        }

                        // võrdlemiseks teha see stringiks
                        // ridade võrdlemiseks
                        string SubTotalString = string.Join("", SubTotal);

                        // tulpade võrdlemiseks
                        string FirstCol = SubTotal[0].ToString() + SubTotal[3].ToString() + SubTotal[6].ToString();
                        string SecondCol = SubTotal[1].ToString() + SubTotal[4].ToString() + SubTotal[7].ToString();
                        string ThirdCol = SubTotal[2].ToString() + SubTotal[5].ToString() + SubTotal[8].ToString();

                        // risti võrdlemiseks

                        string CrossLeft = SubTotal[0].ToString() + SubTotal[4].ToString() + SubTotal[8].ToString();
                        string CrossRight = SubTotal[2].ToString() + SubTotal[4].ToString() + SubTotal[6].ToString();

                        // kontrollida, kas kolm char-i on ridades ja veergudes ning risti samad
                        if(SubTotalString.Substring(0,3) == X ||
                           SubTotalString.Substring(3,3) == X ||
                           SubTotalString.Substring(6,3) == X ||
                           FirstCol == X  ||
                           SecondCol == X ||
                           ThirdCol == X  ||
                           CrossLeft == X || 
                           CrossRight == X)

                        {
                            Console.WriteLine($"{player1} võitis!");
                            GameOn = false;
                        }
                        else if(SubTotalString.Substring(0, 3) == O ||
                                SubTotalString.Substring(3, 3) == O ||
                                SubTotalString.Substring(6, 3) == O ||
                                FirstCol == O  ||
                                SecondCol == O ||
                                ThirdCol == O  ||
                                CrossLeft == O ||
                                CrossRight == O)
                        {
                            Console.WriteLine($"{player2} võitis!");
                            GameOn = false;
                        }
                    }
                }
            }
            Console.WriteLine("Head aega!");
        }
    }
}
